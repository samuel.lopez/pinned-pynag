#!/usr/bin/python

from distutils.core import setup

setup(name='pynag',
      description='Pynag package of the pinned commit',
      version='0.9.1',
      author='Drew Stinnett',
      author_email='drew@drewlink.com',
      url='drewlink.com',
      packages=['pynag'])

